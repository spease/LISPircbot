(defun start-irc-bot (&key debug)
  (format t "Starting IRC MirrorBot~%")
  (let ((s (socket:socket-connect 6667 "card.freenode.net" :external-format :unix))(line nil)(out nil))
        (format s "NICK LISPircbot~%USER LISPircbot 8 * :The Lisp Bot!~%")(force-output s)
	(format t "MirrorBot Connected to server!~%Please wait to authenticate...~%")
        (read-irc s debug)(read s)(read-line s)(read-irc s debug) ;get the first auth, block while it identifies hostname, and then read the MOTD
	(format s "JOIN #MirrorBotTest~%")(force-output s)
	(read-irc s debug)
	(format t "IRC MirrorBot authenticated.~%")
	;(run-irc-bot
	(loop while (EQ (search ".QUIT" line :test #'char-equal) nil) do 
		(setq line (read-line s line))
		(if (EQ (search ":" line) 0) 
			(parse-irc s line)
			(irc-bot-pong s line)))
	(format s "LEAVE #MirrorBotTest~%")(force-output s)
	(read-irc s T)
	(format s "QUIT~%")(force-output s)
	(read-irc s T)
	(close s)))

(defun read-irc (server-stream do-print)
  (let ((line "NOREAD~%"))
  	(loop while (eq :input (socket:socket-status (cons server-stream :input))) do
	   (setq line (read-line server-stream))
	   (if do-print (format t "~A~%" line) nil))))

(defun irc-bot-pong (s line)
 (let ((reponse (subseq line (search "PING" line))))
  if (EQ reponse nil) () (progn
  (format s "PONG ~A~%" response)(force-output s)
  (format t "~A~%" line)))
)

(defun irc-pretty-print (line)
  (let ( (reg (multiple-value-list (regexp:match "^:\\([a-zA-Z0-9]*\\)![a-zA-Z0-9]*@[a-zA-Z0-9.-]* [A-Z]* #\\([a-zA-Z0-9_-]*\\) :\\([a-zA-Z0-9 ?'\":;<>`~._-]*\\)" line)))
	 (nick "")
	 (channel "")
	 (message "") )
	 (if (EQ reg NIL) () (progn
	 (setq nick (regexp:match-string line (second reg))) 
	 (setq channel (regexp:match-string line (third reg)))
	 (setq message (regexp:match-string line (fourth reg)))
	 (format t "~A@~A> ~A~%" nick channel message)
	 (return-from irc-pretty-print (list nick channel message)
)))))

(defun parse-irc (server line)
	(format t line)
	(setq line (irc-pretty-print line))
	(if (EQ line NIL) () (progn
             (let ((reg (multiple-value-list(regexp:match "\\.\\([a-zA-Z0-9_-]*\\) \\(.*\\)" (third line))))
	           (command "")
	           (parameter ""))
		(if (EQ (second reg) NIL) () (progn
	          (setq command (regexp:match-string (third line) (second reg)))
	          (setq parameter (regexp:match-string (third line) (third reg)))
		  (format t "~A|~A~%" command parameter)
                  (if (NOT (EQ (search "SAY" command :test #'char-equal) nil)) (progn (format server "PRIVMSG #MirrorBotTest :~A~%" parameter)
		  (force-output server))())))))))
